import asyncio
from datetime import datetime
import requests
from sqlalchemy import select
from database import AsyncSessionLocal
from views.sellers import Seller
from views.categories import Category as CategoryModel
from sqlalchemy.ext.asyncio import AsyncSession
from loguru import logger
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.products import Product as ProductModel
from views.product_sizes import ProductSize

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


async def async_add_category_to_db(subject_title: str):

    async with AsyncSessionLocal() as session:

        new_category = CategoryModel(
            title=subject_title,
            alias=subject_title,
            parent_id=None,
            is_active=True
        )
        session.add(new_category)
        await session.commit()
        await session.refresh(new_category)

        return new_category


async def get_not_existed_product_sizes(
        seller_products,
        session: AsyncSession,
        nm_infos, 
        company_name):

    sizes_barcodes = {}

    for product in seller_products:
        for size in product.size:
            sizes_barcodes[size.skus[0]] = {
                "nm": product.nmID,
                "size": size
            }

    query = select(ProductSize.wb_barcode).where(
        ProductSize.wb_barcode.in_(list(sizes_barcodes.keys()))
    )
    result = await session.execute(query)
    r = [row[0] for row in result.all()]
    r = [_ for _ in list(sizes_barcodes.keys()) if _ not in r]

    if len(r) == 0:
        logger.info(f"Для {company_name} нет новых размеров]")  # noqa 501
        return

    query = select(ProductModel.id, ProductModel.wb_article).where(
        ProductModel.wb_article.in_([sizes_barcodes[_]['nm'] for _ in r])
    )
    result = await session.execute(query)
    products = {row[1]: row[0] for row in result.all()}

    
    for _ in r:
        info = sizes_barcodes[_]
        if nm_infos.get(info['size'].skus[0]) is None:
            size = ProductSize(
                product_id=products[info['nm']],
                size=info["size"].techSize,
                wb_barcode=info["size"].skus[0],
                volume=0
            )
        else:
            size = ProductSize(
                product_id=products[info['nm']],
                size=info["size"].techSize,
                wb_barcode=info["size"].skus[0],
                volume=nm_infos.get(info['size'].skus[0]).liter
            )
        session.add(size)



def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None



async def get_not_existed_products(seller_products, session):

    query = select(ProductModel.wb_article).\
        where(ProductModel.wb_article.in_([i.nmID for i in seller_products]))
    result = await session.execute(query)
    r = [row[0] for row in result.all()]
    return [product for product in seller_products if product.nmID not in r]


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🔴 КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор карточек товаров"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор карточек товаров"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


@logger.catch(
    onerror=error_handler
)
async def start():

    async with AsyncSessionLocal() as session:

        session: AsyncSession = session
        sellers_cursor_result = await session.execute(select(Seller))
        sellers = sellers_cursor_result.scalars().all()

    for seller in sellers:
        if seller.wb_token is None:
            continue
        print('1')
        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)
        
        if supplier is None:
            continue

        seller_products = wb_manager.get_seller_prducts(supplier)

        async with AsyncSessionLocal() as session:
            print('2')

            categories = {
                i.title: i
                for i in (
                        await session.execute(select(CategoryModel))
                    ).scalars().all()
            }

            products = await get_not_existed_products(seller_products, session)

            for product in products:

                category = categories.get(product.subjectName)
                if category is None:
                    category = await async_add_category_to_db(product.subjectName)  # noqa 501
                    categories[category.title] = category

                photo = product.mediaFiles.get(0)
                if photo is not None:
                    photo = photo.value

                new_product = ProductModel(
                    category_id=None if category is None else category.id,
                    seller_id=seller.id,
                    link_on_site=f"https://www.wildberries.ru/catalog/{product.nmID}/detail.aspx",  # noqa 501
                    hasNote=False,
                    name=product.title,
                    article_china=product.vendorCode,
                    wb_article=product.nmID,
                    nomenclature=str(product.nmID),
                    photo_url=photo,
                    brand=product.brandName,
                    default_image_url=photo
                )

                session.add(new_product)
                logger.info(f"Товар {new_product.name} был сохранён")  # noqa 501
            if len(products) == 0:
                logger.info(f"Для {seller.company_name} нет новых товаров")  # noqa 501
                
            await session.commit()
            nm_infos = wb_manager.get_nomanclatures_info(supplier=supplier)
            nm_infos = {i.barcode: i for i in nm_infos if i.barcode is not None}  # noqa 501
            await get_not_existed_product_sizes(
                seller_products,
                session,
                nm_infos, 
                seller.company_name)
            await session.commit()
    send_log_to_tg()
    


if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        level="DEBUG")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
