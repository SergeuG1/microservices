from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP, Boolean
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship


class Category(Base):

    __tablename__ = "categories"

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey("categories.id"))

    title = Column(String, nullable=False, unique=True)
    alias = Column(String, nullable=False, unique=True)
    is_active = Column(Boolean, nullable=False)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)

    parent = relationship('Category', remote_side=[id])
