import asyncio
from sqlalchemy import select
from database import AsyncSessionLocal
from sqlalchemy.ext.asyncio import AsyncSession

from views.products import Product
from views.sellers import Seller
from views.categories import Category


async def start():
    async with AsyncSessionLocal() as session:

        session: AsyncSession = session
        sellers_cursor_result = await session.execute(select(Product))
        products = sellers_cursor_result.scalars().all()

        for product in products:
            basket = int(product.wb_article / 15000000)
            basket = str(basket) if basket >= 10 else f"0{basket}"
            product.photo_url = "https://basket-{}.wb.ru/vol{}/part{}/{}/images/big/1.jpg".format(
                basket,
                int(product.wb_article / 100000),
                int(product.wb_article / 1000),
                str(product.wb_article))

        await session.commit()


if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
