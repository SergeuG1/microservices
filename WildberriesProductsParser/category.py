from dataclasses import dataclass


@dataclass
class Category:

    parent_id: int
    title: str
    sort: int
    id: int
