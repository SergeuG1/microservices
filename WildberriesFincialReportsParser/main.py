from itertools import islice
import time

import requests
from loguru import logger
from datetime import datetime, timedelta
from sqlalchemy.orm import Session
from database import SessionLocal, settings
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.financial_reports.methods import (
    download_financial_report,
    get_report_ids,
    parse_data_from_file,
    FinancialRecord
)
import gc
from wildberries_seller.supplier import Supplier
from views.financial_reports import FinancialReport
from views.product_sizes import ProductSize
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None



def get_not_existed_records(session: Session, records: list[FinancialRecord]):

    ids = []
    for record in records:
        ids.append(str(record.record_id))

    result = [
        _.realization_report_id
        for _ in session.query(FinancialReport).filter(
            FinancialReport.realization_report_id.in_(ids)
        ).all()
    ]

    return [record for record in records if record.record_id not in result]


def get_product_size(
        session: Session,
        barcode: str) -> ProductSize | None:

    product_size = session.query(ProductSize)\
        .filter(ProductSize.wb_barcode == barcode).first()

    return product_size


def create_report(session: Session, record: FinancialRecord):

    product_size = get_product_size(session, record.barcode)

    if product_size is None:
        return None

    r = FinancialReport(
        product_size_id=product_size.id,
        report_id=str(record.report_id),
        realization_report_id=str(record.record_id),
        gi_id=record.delivery_number,
        subject_name=record.subject,
        sa_name=record.suppliers_article,
        ts_name=record.size,
        doc_type_name=record.document_type,
        supplier_oper_name=record.payment_justification,
        order_dt=record.order_date,
        sale_dt=record.sale_date,
        quantity=record.count,
        retail_price=record.retail_price,
        product_discount_for_report=record.total_agreed_discount,
        supplier_promo=record.promocode,
        retail_price_withdisc_rub=record.price_by_discount,
        retail_amount=record.wb_item_realization,
        ppvz_spp_prc=record.regular_customer_discount,
        rating_kvw_prc_decr=record.rating_kvv_decr,
        action_kvw_prc_decr=record.action_kvv_decr,
        commission_percent=record.kvv_size,
        ppvz_kvw_prc_base=record.kvv_base_size,
        ppvz_kvw_prc=record.total_kvv,
        ppvz_sales_commission=record.sales_remuneration,
        ppvz_reward=record.pvz_compensation,
        acquiring_fee=record.acquiring_compensation,
        ppvz_vw=record.wildberries_reward_without_nds,
        ppvz_vw_nds=record.wildberries_reward_nds,
        ppvz_for_pay=record.transfer_to_seller,
        delivery_amount=record.deliveries_count,
        return_amount=record.refund_count,
        delivery_rub=record.delivery_services,
        penalty=record.total_fines_amount,
        additional_payment=record.surcharges,
        bonus_type_name=record.type,
        sticker_id=record.sticker_mp,
        acquiring_bank=record.acquiring_bank_name,
        ppvz_office_id=record.office_number,
        ppvz_office_name=record.delivery_office_name,
        ppvz_inn=str(record.partners_inn),
        ppvz_supplier_name=record.partner,
        office_name=record.warehouse,
        site_country=record.country,
        gi_box_type_name=record.boxes_type,
        declaration_number=record.customs_declaration_number,
        kiz=record.marking_code,
        shk_id=str(record.shk),
        rid=str(record.rid),
        srid=str(record.srid),
        transfer_expences=record.transfer_expences,
        transfer_org=str(record.transfer_org)
    )
    try:
        session.add(r)
        session.commit()
    except Exception as e:
        session.rollback()
        logger.critical(f"Ошибка сохранения записей финансового отчёта {e}")  # noqa 501
    return r


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор финансовых отчётов"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор финансовых отчётов"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)



@logger.catch(onerror=error_handler)
def start():

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    today = datetime.now()
    last_sun = today - timedelta(days=today.weekday()+1)
    start_date = last_sun - timedelta(days=6)
    end_date = last_sun

    for seller in sellers:
        if seller.wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue

        report_ids = get_report_ids(
            wb_manager,
            supplier,
            start_date=start_date,
            end_date=end_date)

        def chunks(generator, size=1000):
            while True:
                chunk = list(islice(generator, size))
                if not chunk:
                    break
                yield chunk

        with SessionLocal() as session:

            for report_id in report_ids:

                file_path = download_financial_report(
                    wb_manager,
                    supplier,
                    report_id,
                    settings["FILE_STORAGE"])
                records = parse_data_from_file(report_id, file_path)
                for chunk in chunks(records, size=1000):
                    records = get_not_existed_records(session, chunk)
                    for record in records:
                        r = create_report(session, record)
                        if r is None:
                            continue
                        logger.info(f"Запись №{r.realization_report_id} финансового отчёта №{r.report_id} под ID {r.id}")  # noqa 501
                del records
                gc.collect()
                time.sleep(5)
            
            try:
                session.commit()
                seller.wb_token = wb_manager.refresh_token()
            except Exception as e:
                session.rollback()
                logger.critical(f"Не удалось сохранить финансового отчёт из-за {e}")  # noqa 501
            time.sleep(15)
    send_log_to_tg()

if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
