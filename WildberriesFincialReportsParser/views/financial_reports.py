from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, TIMESTAMP, Float, String
from sqlalchemy.sql import func


class FinancialReport(Base):

    __tablename__ = "financial_reports"

    id = Column(Integer, primary_key=True)
    product_size_id = Column(Integer, ForeignKey("product_sizes.id"), nullable=False) # noqa 501

    report_id = Column(String, nullable=True)
    realization_report_id = Column(String, nullable=True, unique=True)
    gi_id = Column(Integer, nullable=True)
    subject_name = Column(String, nullable=True)
    sa_name = Column(String, nullable=True)
    retail_amount = Column(Float, nullable=True)
    ts_name = Column(String, nullable=True)
    doc_type_name = Column(String, nullable=True)
    supplier_oper_name = Column(String, nullable=True)
    order_dt = Column(TIMESTAMP, nullable=True)
    sale_dt = Column(TIMESTAMP, nullable=True)
    quantity = Column(Integer, nullable=True)
    retail_price = Column(Float, nullable=True)
    product_discount_for_report = Column(Float, nullable=True)
    supplier_promo = Column(Integer, nullable=True)
    retail_price_withdisc_rub = Column(Float, nullable=True)
    rating_kvw_prc_decr = Column(Float, nullable=True)
    action_kvw_prc_decr = Column(Float, nullable=True)
    ppvz_spp_prc = Column(Float, nullable=True)
    commission_percent = Column(Float, nullable=True)
    ppvz_kvw_prc_base = Column(Float, nullable=True)
    ppvz_kvw_prc = Column(Float, nullable=True)
    ppvz_sales_commission = Column(Float, nullable=True)
    ppvz_reward = Column(Float, nullable=True)
    acquiring_fee = Column(Float, nullable=True)
    ppvz_vw = Column(Float, nullable=True)
    ppvz_vw_nds = Column(Float, nullable=True)
    ppvz_for_pay = Column(Float, nullable=True)
    delivery_amount = Column(Integer, nullable=True)
    return_amount = Column(Integer, nullable=True)
    delivery_rub = Column(Float, nullable=True)
    penalty = Column(Float, nullable=True)
    additional_payment = Column(Float, nullable=True)
    bonus_type_name = Column(String, nullable=True)
    sticker_id = Column(String, nullable=True)
    acquiring_bank = Column(String, nullable=True)
    ppvz_office_id = Column(Integer, nullable=True)
    ppvz_office_name = Column(String, nullable=True)
    ppvz_inn = Column(String, nullable=True)
    ppvz_supplier_name = Column(String, nullable=True)
    office_name = Column(String, nullable=True)
    site_country = Column(String, nullable=True)
    gi_box_type_name = Column(String, nullable=True)
    declaration_number = Column(String, nullable=True)
    kiz = Column(String, nullable=True)
    shk_id = Column(String, nullable=True)
    rid = Column(String, nullable=True)
    srid = Column(String, nullable=True)
    transfer_expences = Column(Float, nullable=True)
    transfer_org = Column(String, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
