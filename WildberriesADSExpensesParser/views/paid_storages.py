from datetime import datetime
from database import Base
from sqlalchemy import Column, Integer, ForeignKey, Float, TIMESTAMP
from sqlalchemy.sql import func


class PaidStorage(Base):

    __tablename__ = "paid_storages"

    id = Column(Integer, primary_key=True, index=True)
    seller_id = Column(
        Integer, ForeignKey("sellers.id"), nullable=True, index=True)
    total_rub = Column(Float, default=0, nullable=False)
    total_shk = Column(Integer, default=0, nullable=False)
    total_monopallets = Column(Integer, default=0, nullable=False)

    boxes_lt_five_liters_rub = Column(Float, default=0, nullable=False)
    boxes_lt_five_liters_shk = Column(Integer, default=0, nullable=False)
    boxes_lt_five_liters_monopallets = Column(Integer, default=0, nullable=False)

    boxes_gt_five_liters_rub = Column(Float, default=0, nullable=False)
    boxes_gt_five_liters_shk = Column(Integer, default=0, nullable=False)
    boxes_gt_five_liters_monopallets = Column(Integer, default=0, nullable=False)

    boxes_without_dimensions_rub = Column(Float, default=0, nullable=False)
    boxes_without_dimensions_shk = Column(Integer, default=0, nullable=False)
    boxes_without_dimensions_monopallets = Column(Integer, default=0, nullable=False)

    boxes_without_photo_rub = Column(Float, default=0, nullable=False)
    boxes_without_photo_shk = Column(Integer, default=0, nullable=False)
    boxes_without_photo_monopallets = Column(Integer, default=0, nullable=False)

    main_pallets_rub = Column(Float, default=0, nullable=False)
    main_pallets_shk = Column(Integer, default=0, nullable=False)
    main_pallets_monopallets = Column(Integer, default=0, nullable=False)

    subsort_of_pallets_rub = Column(Float, default=0, nullable=False)
    subsort_of_pallets_shk = Column(Integer, default=0, nullable=False)
    subsort_of_pallets_monopallets = Column(Integer, default=0, nullable=False)

    start_date = Column(TIMESTAMP, nullable=False)
    end_date = Column(TIMESTAMP, nullable=False)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
