from datetime import datetime
from database import Base
from sqlalchemy import Column, Integer, TIMESTAMP, Double, String
from sqlalchemy.sql import func


class AdsExpenses(Base):

    __tablename__ = "ads_expenses"

    id = Column(Integer, primary_key=True)
    advert_id = Column(Integer, nullable=False)
    name = Column(String, nullable=False)
    date = Column(TIMESTAMP, nullable=False)
    cost = Column(Double, nullable=False)
    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
    seller_id = Column(Integer, nullable=False)