from datetime import datetime
from database import Base
from sqlalchemy import Column, Float, ForeignKey, Integer, TIMESTAMP
from sqlalchemy.sql import func


class SelfStorageCost(Base):

    __tablename__ = "self_storage_costs"

    id = Column(Integer, primary_key=True)
    seller_id = Column(Integer, ForeignKey("sellers.id"), nullable=True)
    date_from = Column(TIMESTAMP, nullable=False)
    date_to = Column(TIMESTAMP, nullable=False)
    total_storage = Column(Float, nullable=False)
    total_cost = Column(Float, nullable=False)
    one_liter_cost = Column(Float, nullable=False)
    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
