import time

import requests
from loguru import logger
from datetime import datetime, timedelta
from database import SessionLocal
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.ads_expenses import AdsExpenses
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🔴 КРИТИЧЕСКАЯ ОШИБКА 1\nМодуль: NiiN сбор рекламных затрат"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам 1\nМодуль: NiiN сбор рекламной затраты"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def sort_with_dates(costs: list[AdsExpenses]) -> list[AdsExpenses]:
    temp_costs = []
    date = (datetime.today().replace(hour=0, microsecond=0, second=0, minute=0) - timedelta(days=1)).strftime('%d-%m-%Y')
    for cost in costs:
        if cost.date.strftime('%d-%m-%Y') == date:
            temp_costs.append(cost)
    
    return temp_costs


@logger.catch(
        onerror=error_handler
)
def start():

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    for seller in sellers:

        if seller.wb_token is None or \
                seller.ads_wb_token is None or \
                seller.cmp_wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT,
            ads_wb_token=seller.cmp_wb_token)
        
        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue

        all_expenses = wb_manager.get_ads_expenses(supplier)
        if all_expenses is None:
            continue
        all_expenses = sort_with_dates(all_expenses)
        if len(all_expenses) == 0:
            continue
        
        with SessionLocal() as session:
            session.add_all([
                AdsExpenses(
                    advert_id=_.advert_id,
                    name=_.name,
                    date=_.date,
                    cost=_.sum,
                    created_at=datetime.now(),
                    updated_at=datetime.now(),
                    deleted_at=None,
                    seller_id=seller.idd
                )
                for _ in all_expenses
            ])

            try:
                session.commit()
                logger.info(f"Были записаны новые данные по рекламе {seller.company_name} штук {len(all_expenses)}")
            except Exception as e:
                session.rollback()
                logger.critical(f"Не удалось сохранить записи по рекламе {e} {e.args}")  # noqa 501
        time.sleep(10)
    send_log_to_tg()


if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
