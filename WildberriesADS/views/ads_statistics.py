from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, TIMESTAMP, Float
from sqlalchemy.sql import func


class AdsStat(Base):

    __tablename__ = "ads_stats"

    id = Column(Integer, primary_key=True)
    advert_id = Column(Integer)
    product_id = Column(Integer, ForeignKey("products.id"), nullable=True)
    frequency = Column(Float)
    views = Column(Integer)
    clicks = Column(Integer)
    ctr = Column(Float)
    cpc = Column(Float)
    added_to_cart = Column(Integer)
    orders = Column(Integer)
    sum = Column(Integer)

    date = Column(TIMESTAMP, nullable=False)
    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
