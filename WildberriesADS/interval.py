import time

import requests
from loguru import logger
from datetime import datetime
from database import SessionLocal
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.ads_statistics import AdsStat
from views.products import Product
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        company_name: str) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for _ in suppliers:
        if _.full_name == company_name:
            return _
    return None


def get_list_of_campaigns(token: str):

    def get_count() -> int:

        url = "https://advert-api.wb.ru/adv/v0/count"
        headers = {
            "Authorization": token
        }

        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            return 0
        return response.json()["all"]

    def get_adverts(count=100):
        url = "https://advert-api.wb.ru/adv/v0/adverts"
        headers = {
            "Authorization": token
        }
        params = {
            "limit": count,
        }

        response = requests.get(url, headers=headers, params=params)
        if response.status_code != 200:
            return

        result = response.json()

        return result

    count = get_count()
    if count == 0:
        return []

    result: list = get_adverts(count)

    if result is None:
        return []

    return result


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор отчёта по скидкам"}  # noqa 501
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


@logger.catch()
def start():

    # получение селлеров

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    for seller in sellers[-1:]:

        # проверки селлера

        if seller.wb_token is None or \
                seller.ads_wb_token is None or \
                seller.cmp_wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT,
            ads_wb_token=seller.cmp_wb_token)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller.company_name)

        if supplier is None:
            continue

        all_adverts = get_list_of_campaigns(token=seller.ads_wb_token)
        if all_adverts is None:
            continue

        all_adverts = [advert['advertId'] for advert in all_adverts]

        with SessionLocal() as session:
            for idx, advert_id in enumerate(all_adverts):

                print(f"{idx} / {len(all_adverts)}")

                advert = wb_manager.get_full_statistic(advert_id, supplier)
                time.sleep(2)
                if advert.short_statistic is None:
                    continue

                for day in advert.short_statistic.days:
                    product_info: dict[int, AdsStat] = {}
                    for device in day.apps:
                        for nm in device.nm:
                            if product_info.get(nm.nmId) is None:

                                product_id = session.query(Product.id)\
                                    .filter(Product.wb_article == nm.nmId)\
                                    .scalar()

                                product_info[nm.nmId] = AdsStat(
                                    advert_id=advert_id,
                                    product_id=product_id,
                                    frequency=nm.frq,
                                    views=nm.views,
                                    clicks=nm.clicks,
                                    ctr=nm.ctr,
                                    cpc=nm.cpc,
                                    added_to_cart=nm.atbs,
                                    orders=nm.orders,
                                    sum=nm.orders,
                                    date=datetime.strptime(day.date, "%Y-%m-%dT%H:%M:%S+03:00")  # noqa 501
                                )
                                continue

                            product_info[nm.nmId].frequency += nm.frq
                            product_info[nm.nmId].orders += nm.orders
                            product_info[nm.nmId].views += nm.views
                            product_info[nm.nmId].clicks += nm.clicks
                            product_info[nm.nmId].ctr += nm.ctr
                            product_info[nm.nmId].cpc += nm.cpc
                            product_info[nm.nmId].added_to_cart += nm.atbs
                            product_info[nm.nmId].sum += nm.sum
                    session.add_all(list(product_info.values()))

            try:
                session.commit()
                logger.info(f"Были записаны новые данные по рекламе {seller.company_name} штук")  # noqa 501
            except Exception as e:
                session.rollback()
                logger.critical(f"Не удалось сохранить записи по рекламе {e}")  # noqa 501

        time.sleep(10)


if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
