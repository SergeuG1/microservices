from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, TIMESTAMP, Float
from sqlalchemy.sql import func


class WbStockCity(Base):

    __tablename__ = "wb_stocks_cities"

    id = Column(Integer, primary_key=True)
    wb_stock_id = Column(Integer, ForeignKey("wb_stocks.id"), nullable=False)
    city_id = Column(Integer, ForeignKey("cities.id"), nullable=False)

    value = Column(Float, nullable=False)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
