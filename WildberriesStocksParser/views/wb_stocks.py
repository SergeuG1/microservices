from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP
from sqlalchemy.sql import func


class WbStock(Base):

    __tablename__ = "wb_stocks"

    id = Column(Integer, primary_key=True)
    product_size_id = Column(Integer, ForeignKey("product_sizes.id"),
                             nullable=False)

    storage_volume_liters = Column(String, nullable=True)
    delivering_to_client = Column(Integer, nullable=True)
    delivering_from_client = Column(Integer, nullable=True)
    total_storage_count = Column(Integer, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
