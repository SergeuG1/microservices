from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import dotenv_values, find_dotenv
from sqlalchemy.ext.asyncio import AsyncSession


settings = dotenv_values(find_dotenv())
driver_path: str = "postgresql+asyncpg://{}:{}@{}:5432/{}".format(
    settings["DB_LOGIN"],
    settings["DB_PASSWORD"],
    settings["DB_HOST"],
    settings["DB_NAME"])


engine = create_async_engine(url=driver_path)

AsyncSessionLocal = sessionmaker(
    bind=engine,
    class_=AsyncSession,
    expire_on_commit=False)
Base = declarative_base()
