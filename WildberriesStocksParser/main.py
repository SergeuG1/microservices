from loguru import logger
from datetime import datetime, timedelta
import requests

from sqlalchemy import select
from database import AsyncSessionLocal, AsyncSession, settings
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from wildberries_seller.stock_balances import StockBalanceRecord
import asyncio
from views.cities import City
from views.countries import Country
from views.product_sizes import ProductSize

from views.sellers import Seller
from views.wb_stocks import WbStock
from views.wb_stocks_cities import WbStockCity
from cities import CITIES


USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None



async def create_country(session: AsyncSession, value: str):

    query = select(Country)\
        .where(Country.title == value)
    country = (await session.execute(query))\
        .scalars().first()

    if country is not None:
        return country

    country = Country(title=value)
    try:
        session.add(country)
        await session.commit()
        await session.refresh(country)
        logger.info(f"Страна {country.title} сохранилась под ID {country.id}")  # noqa 501
    except Exception as e:
        await session.rollback()
        logger.critical(f"Не удалось создать страну {value} {e}")
        return None

    return country


async def create_city(session: AsyncSession, value: str, country: str):

    query = select(City)\
        .where(City.title == value)
    city = (await session.execute(query))\
        .scalars().first()

    if city is not None:
        return city

    country = await create_country(session, country)
    city = City(title=value, country_id=country.id)

    try:
        session.add(city)
        await session.commit()
        await session.refresh(city)
        logger.info(f"Город {city.title} страны {country.title} сохранился под ID {city.id}")  # noqa 501
    except Exception as e:
        await session.rollback()
        logger.critical(f"Не удалось создать город {value} страны {country.title} {e}")  # noqa 501
        return None
    return city


async def create_city_stock(
        session: AsyncSession,
        city_id: int,
        stock_id: int,
        value: int):

    query = select(WbStockCity)\
        .where(
            WbStockCity.city_id == city_id and
            WbStockCity.wb_stock_id == stock_id)
    city_stock = (await session.execute(query))\
        .scalars().first()

    if city_stock is not None:
        return city_stock

    city_stock = WbStockCity(
        city_id=city_id,
        wb_stock_id=stock_id,
        value=value)

    try:
        session.add(city_stock)
    except Exception as e:
        await session.rollback()
        logger.critical(f"Не удалось сохранить остатки отчёта {stock_id} города {city_id} {e}")  # noqa 501
        return None
    return city_stock


async def generate_cities(session: AsyncSession):

    result = {
        city: await create_city(session, city, country)
        for city, country in CITIES.items()
    }

    return result


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🔴 КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор остатков на складе"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        # requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор остатков на складе"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        # requests.post(url, data=data, files=files)




async def is_record_already_exists(session: AsyncSession, product_size_id: int):

    today = datetime.now()
    today = datetime(today.year, today.month, today.day, 0, 0, 0)
    result = (await session.execute(
        select(WbStock).where(
            WbStock.product_size_id == product_size_id,
            WbStock.created_at.between(
                today,
                today + timedelta(hours=23, minutes=59, seconds=59)
            )
        )
    )).scalars().first()
    return result



@logger.catch(
        onerror=error_handler
)
async def start():
    async with AsyncSessionLocal() as session:

        session: AsyncSession = session
        sellers_cursor_result = await session.execute(select(Seller))
        sellers = sellers_cursor_result.scalars().all()

    for seller in sellers:
        if seller.wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue
        
        stocks: list[StockBalanceRecord] = wb_manager.get_stocks_data(
            supplier,
            settings["FILE_STORAGE"])

        async with AsyncSessionLocal() as session:
            cities: dict[str, City] = await generate_cities(session)

            for stock in stocks:
                query = select(ProductSize)\
                    .where(ProductSize.wb_barcode == stock.barcode)
                product_size = (await session.execute(query))\
                    .scalars().first()

                if product_size is None:
                    continue

                wb_stock = await is_record_already_exists(session, product_size.id)

                if wb_stock is None:
                    wb_stock = WbStock(
                        product_size_id=product_size.id,
                        storage_volume_liters=stock.brand,
                        delivering_to_client=stock.goods_transit_to_customer if stock.goods_transit_to_customer is not None else 0,
                        delivering_from_client=stock.goods_transit_from_customer if stock.goods_transit_from_customer is not None else 0,
                        total_storage_count=stock.total_warehouse if stock.total_warehouse is not None else 0,
                    )

                    try:
                        session.add(wb_stock)
                        # await session.commit()
                        # await session.refresh(wb_stock)
                        logger.info(f"Запись №{wb_stock.id} сохранена под ID {wb_stock.id}")  # noqa 501
                    except Exception as e:
                        await session.rollback()
                        logger.critical(f"Ошибка сохранения записи №{wb_stock.id} {e}")  # noqa 501

                await asyncio.gather(*[
                    create_city_stock(
                        session=session,
                        city_id=cities[city].id,
                        stock_id=wb_stock.id,
                        value=value)
                    for city, value in stock.cities.items()
                    if value is not None
                ])

                # await session.commit()

            seller.wb_token = wb_manager.refresh_token()
            # await session.commit()
            await asyncio.sleep(15)
    send_log_to_tg()

if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        level="DEBUG")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
