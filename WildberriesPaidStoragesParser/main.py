import time
from loguru import logger
from datetime import datetime, timedelta
import requests

from sqlalchemy import select, text
from database import AsyncSessionLocal, AsyncSession
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
import asyncio
from views.self_storage_cost import SelfStorageCost

from views.sellers import Seller
from views.paid_storages import PaidStorage


USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


async def get_total_rub(
        session,
        date: datetime,
        seller_id: int):

    query = '''
        select total_rub
        from public.paid_storages
        where start_date = '{0}' and seller_id = {1}
    '''

    query = text(query.format(date.strftime("%Y-%m-%d %H:%M:%S"), seller_id))

    result = await session.execute(query)
    data = [r._mapping for r in result]

    total_costs = 0
    if len(data) != 0:
        total_costs = data[0]["total_rub"]
    if total_costs is None:
        total_costs = 0

    return total_costs


async def get_total_storage(
        session,
        date: datetime,
        seller_id: int):

    query = '''
        select sum(total_volume) as total_storage
        from (
            select total_storage_count, volume, (
                total_storage_count * volume
            ) as total_volume
            from wb_stocks
            inner join product_sizes on product_size_id = product_sizes.id
            where wb_stocks.created_at between '{0}' and '{1}' and product_size_id in (
                select id
                from product_sizes
                where product_id in (
                    select id from products where seller_id = {2}
                )
            ) and total_storage_count > 0
        ) as foo
    '''

    query = text(query.format(
        date.strftime("%Y-%m-%d 00:00:00"),
        date.strftime("%Y-%m-%d 23:59:59"),
        seller_id))
    result = await session.execute(query)
    data = [r._mapping for r in result]

    total_storage = 0
    if len(data) != 0:
        total_storage = data[0]["total_storage"]
    if total_storage is None:
        total_storage = 0

    return total_storage




def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None




def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор отчёта стоимости хранения"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль:  NiiN сбор отчёта стоимости хранения"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


@logger.catch(onerror=error_handler)
async def start():
    async with AsyncSessionLocal() as session:

        session: AsyncSession = session
        sellers_cursor_result = await session.execute(select(Seller))
        sellers = sellers_cursor_result.scalars().all()

    for seller in sellers:
        if seller.wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue

        date = datetime.today().replace(hour=0, microsecond=0, second=0, minute=0) - timedelta(days=1)

        async with AsyncSessionLocal() as session:

            paid_storage = wb_manager.get_paid_storage(
                start_date=date,
                end_date=date,
                supplier=supplier)

            storage = PaidStorage(
                seller_id=seller.id,

                total_rub=paid_storage.total.rub,
                total_shk=paid_storage.total.shk,
                total_monopallets=paid_storage.total.monopallets,

                boxes_lt_five_liters_rub=paid_storage.boxes_lt_five_liters.rub,
                boxes_lt_five_liters_shk=paid_storage.boxes_lt_five_liters.shk,
                boxes_lt_five_liters_monopallets=paid_storage.boxes_lt_five_liters.monopallets,

                boxes_gt_five_liters_rub=paid_storage.boxes_gt_five_liters.rub,
                boxes_gt_five_liters_shk=paid_storage.boxes_gt_five_liters.shk,
                boxes_gt_five_liters_monopallets=paid_storage.boxes_gt_five_liters.monopallets,

                boxes_without_dimensions_rub=paid_storage.boxes_without_dimensions.rub,
                boxes_without_dimensions_shk=paid_storage.boxes_without_dimensions.rub,
                boxes_without_dimensions_monopallets=paid_storage.boxes_without_dimensions.rub,

                boxes_without_photo_rub=paid_storage.boxes_without_photo.rub,
                boxes_without_photo_shk=paid_storage.boxes_without_photo.rub,
                boxes_without_photo_monopallets=paid_storage.boxes_without_photo.rub,

                main_pallets_rub=paid_storage.main_pallets.rub,
                main_pallets_shk=paid_storage.main_pallets.shk,
                main_pallets_monopallets=paid_storage.main_pallets.monopallets,

                subsort_of_pallets_rub=paid_storage.subsort_of_pallets.rub,
                subsort_of_pallets_shk=paid_storage.subsort_of_pallets.shk,
                subsort_of_pallets_monopallets=paid_storage.subsort_of_pallets.monopallets,

                start_date=date,
                end_date=date,

                created_at=datetime.now(),
                updated_at=datetime.now(),
                deleted_at=None
            )
            session.add(storage)
            try:
                 await session.commit()
            except Exception as e:
                session.rollback()
                logger.critical(f"Не удалось сохранить из-за {e}")  # noqa 501

            logger.info(f"Данные {seller.company_name} за {date} успешно сохранились")

            total_costs = await get_total_rub(session, storage.start_date, storage.seller_id)
            total_storage = await get_total_storage(session, storage.start_date + timedelta(days=1), storage.seller_id)

            one_litr = 0 if total_costs == 0 or total_storage == 0 else total_costs / total_storage

            r = SelfStorageCost(
                seller_id=storage.seller_id,
                date_from=storage.start_date,
                date_to=storage.end_date,
                total_storage=total_storage,
                total_cost=total_costs,
                one_liter_cost=one_litr,
                created_at=datetime.now(),
                updated_at=datetime.now(),
                deleted_at=None
            )

            session.add(r)
            try:
                 await session.commit()
            except Exception as e:
                session.rollback()
                logger.critical(f"Не удалось сохранить из-за {e}")  # noqa 501


            time.sleep(10)

if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        level="DEBUG",
        compression="zip")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
