import time

import requests
from loguru import logger
from datetime import datetime
from sqlalchemy.orm import Session
from database import SessionLocal, settings
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.discounts import Discount
from views.product_sizes import ProductSize
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None



def get_product_size(
        session: Session,
        barcode: str) -> ProductSize | None:

    product_size = session.query(ProductSize)\
        .filter(ProductSize.wb_barcode == barcode).first()

    return product_size


def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🔴КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор отчёта по скидкам"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        # requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор отчёта по скидкам"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        # requests.post(url, data=data, files=files)


@logger.catch(
        onerror=error_handler
        )
def start():

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    for seller in sellers:
        if seller.wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue

        discounts = wb_manager.get_discounts_info(
            supplier,
            settings["FILE_STORAGE"])

        product_sizes_ids: dict[str, int] = {}

        with SessionLocal() as session:
            added_count = 0
            for d in discounts:
                product_size_id = product_sizes_ids.get(d.last_barcode)
                if product_size_id is None:
                    product_size = get_product_size(
                        session, d.last_barcode)
                    if product_size is None:
                        continue
                    product_size_id = product_size.id
                    product_sizes_ids[d.last_barcode] = product_size_id
                if d.turnaround == '':
                    d.turnaround = 0

                discount = Discount(
                    product_size_id=product_size_id,
                    collection=d.collection,
                    days_on_site=d.days_on_site,
                    illiquid=d.illiquidity,
                    date_illiquid=d.date_appearance_illiquidity,
                    turnover=d.turnaround,
                    quantity=d.remainder_of_goods,
                    new_retail=d.new_retail_price,
                    current_retail=d.current_retail_price,
                    actual_discount=d.current_discount_site,
                    recommended_discount=d.recommended_discount,
                    discount=d.agreed_discount,
                    current_discount_by_promo=d.current_promo_code_discount,  # noqa 501
                    new_discount_by_promo=d.new_promotional_code_discount
                )
                session.add(discount)
                added_count += 1
            try:
                # session.commit()
                logger.info(f"Были записаны новые данные по скидкам {seller.company_name} {added_count} штук")  # noqa 501
            except Exception as e:
                # session.rollback()
                logger.critical(f"Не удалось сохранить записи по скидкам {e}")  # noqa 501

            # seller.wb_token = wb_manager.refresh_token()
            # session.commit()
            time.sleep(10)
    send_log_to_tg()    

if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
