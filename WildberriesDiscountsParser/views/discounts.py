from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP, Float


class Discount(Base):

    __tablename__ = "discount"

    id = Column(Integer, primary_key=True)
    product_size_id = Column(Integer, ForeignKey("product_sizes.id"), nullable=False)  # noqa 501
    collection = Column(String, nullable=True)
    days_on_site = Column(Integer, nullable=True)
    illiquid = Column(String, nullable=True)
    date_illiquid = Column(TIMESTAMP, nullable=True)
    turnover = Column(Integer, nullable=True)
    quantity = Column(Integer, nullable=True)
    new_retail = Column(Integer, nullable=True)
    current_retail = Column(Integer, nullable=True)
    actual_discount = Column(Float, nullable=True)
    recommended_discount = Column(Float, nullable=True)
    discount = Column(Float, nullable=True)
    current_discount_by_promo = Column(Float, nullable=True)
    new_discount_by_promo = Column(Float, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
