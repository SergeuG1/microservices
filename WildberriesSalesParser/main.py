import time
from loguru import logger
from datetime import datetime, timedelta

import requests
from database import SessionLocal, settings
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.product_sizes import ProductSize
from views.sale import Sale
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        seller) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for supplier in suppliers:
        if supplier.old_id == int(seller.wb_article):
            return supplier 
    return None



def error_handler(exception: BaseException):

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор отчёта по продажам"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)


def send_log_to_tg():

    BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
    CHAT_ID = -935052991
    file_name = datetime.now().strftime("%d_%m_%Y.log")

    data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор отчёта по продажам"}
    logger.stop()
    url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
    with open('logs/' + file_name, 'rb') as f:
        files = {'document': f}
        requests.post(url, data=data, files=files)




@logger.catch(onerror=error_handler)
def start():

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    date = datetime.today() - timedelta(days=1)

    for seller in sellers:
        if seller.wb_token is None:
            continue

        wb_manager = WildberriesManager(
            wb_token=seller.wb_token,
            user_agent=USER_AGENT)

        supplier: Supplier | None = find_wb_supplier(
            wb_manager, seller)

        if supplier is None:
            continue

        file = wb_manager.download_daily_salles_report(
            supplier=supplier,
            start_date=date,
            end_date=date,
            files_storage_path=settings["FILE_STORAGE"]
        )

        data: list[WildberriesManager.SalleRecord] = wb_manager.parse_salles_report(file)
        with SessionLocal() as session:
            product_sizes = session.query(ProductSize)\
                .filter(
                    ProductSize.wb_barcode.in_(
                        list(set([_.barcode for _ in data]))))
            product_sizes = {size.wb_barcode: size for size in product_sizes}

            result = [
                Sale(
                    product_size_id=product_sizes[record.barcode].id,
                    brand=record.brand,
                    category=record.subject,
                    season=record.season,
                    collection=record.collection,
                    name=record.name,
                    seller_article=record.seller_article,
                    wb_article=record.wb_article,
                    barcode=record.barcode,
                    size=record.size,
                    contract=record.contract,
                    wb_storage_id=record.warehouse,
                    income_count=record.receipts_count,
                    ordered_count=record.ordered_count,
                    ordered_sum=record.order_amount_minus_wb_comission,
                    bought_count=record.redeemed_count,
                    cach_to_send=record.to_enumeration,
                    current_remind=record.remains_count,
                    report_created_date_from=date,
                    report_created_date_to=date,
                    created_at=datetime.now(),
                    updated_at=datetime.now(),
                    deleted_at=datetime.now()
                )
                for record in data
                if product_sizes.get(record.barcode) is not None
            ]

            session.add_all(result)
            session.commit()
            logger.info(f"Отчёт {seller.company_name} по продажам за {date} - {date} был успешно сохранён")
            time.sleep(5)
        seller.wb_token = wb_manager.refresh_token()
        try:
            session.commit()
        except Exception as e:
            session.rollback()
            logger.critical(f"Не удалось сохранить из-за {e}")  # noqa 501
        time.sleep(15)
    send_log_to_tg()

if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
