import time
from loguru import logger
from datetime import datetime, timedelta
from database import SessionLocal, settings
from wildberries_seller.wildberries_module import WildberriesManager
from wildberries_seller.supplier import Supplier
from views.product_sizes import ProductSize
from views.sale import Sale
from views.sellers import Seller

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501


def find_wb_supplier(
        wb: WildberriesManager,
        company_name: str) -> Supplier | None:

    suppliers: list[Supplier] = wb.get_suppliers()
    print(suppliers)

    for _ in suppliers:
        if _.full_name == company_name:
            return _
    return None


@logger.catch()
def start():

    with SessionLocal() as session:
        sellers = session.query(Seller).all()

    start_date = datetime(year=2023, month=4, day=23)
    end_date = datetime(year=2023, month=4, day=24)
    range_days = (end_date - start_date).days

    for f in range(0, range_days):
        date = end_date - timedelta(days=f)
        for seller in sellers:
            if seller.wb_token is None:
                continue

            wb_manager = WildberriesManager(
                wb_token=seller.wb_token,
                user_agent=USER_AGENT)

            supplier: Supplier | None = find_wb_supplier(
                wb_manager, seller.company_name)

            if supplier is None:
                continue

            file = wb_manager.download_daily_salles_report(
                supplier=supplier,
                start_date=date,
                end_date=date,
                files_storage_path=settings["FILE_STORAGE"]
            )

            data: list[WildberriesManager.SalleRecord] = wb_manager.parse_salles_report(file)
            with SessionLocal() as session:
                product_sizes = session.query(ProductSize)\
                    .filter(
                        ProductSize.wb_barcode.in_(
                            list(set([_.barcode for _ in data]))))
                product_sizes = {size.wb_barcode: size for size in product_sizes}

                result = [
                    Sale(
                        product_size_id=product_sizes[record.barcode].id,
                        brand=record.brand,
                        category=record.subject,
                        season=record.season,
                        collection=record.collection,
                        name=record.name,
                        seller_article=record.seller_article,
                        wb_article=record.wb_article,
                        barcode=record.barcode,
                        size=record.size,
                        contract=record.contract,
                        wb_storage_id=record.warehouse,
                        income_count=record.receipts_count,
                        ordered_count=record.ordered_count,
                        ordered_sum=record.order_amount_minus_wb_comission,
                        bought_count=record.redeemed_count,
                        cach_to_send=record.to_enumeration,
                        current_remind=record.remains_count,
                        report_created_date_from=date,
                        report_created_date_to=date,
                        created_at=datetime.now(),
                        updated_at=datetime.now(),
                        deleted_at=datetime.now()
                    )
                    for record in data
                    if product_sizes.get(record.barcode) is not None
                ]

                session.add_all(result)
                session.commit()
                logger.info(f"Отчёт {seller.company_name} по продажам за {date} - {date} был успешно сохранён")
                time.sleep(5)
        seller.wb_token = wb_manager.refresh_token()
        session.commit()
        time.sleep(15)


if __name__ == "__main__":

    logger.add(
        "logs/{}.log".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()
