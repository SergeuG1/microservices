certifi==2022.12.7
charset-normalizer==3.0.1
colorama==0.4.6
et-xmlfile==1.1.0
greenlet==2.0.2
idna==3.4
loguru==0.6.0
memory-profiler==0.61.0
openpyxl==3.1.1
psutil==5.9.4
pydantic==1.10.5
python-dotenv==1.0.0
requests==2.28.2
SQLAlchemy==2.0.6
typing_extensions==4.5.0
tzdata==2022.7
urllib3==1.26.14
wildberries-seller @ git+http://gitlab.com/nikitakunov8/wildberries-saller-module.git
win32-setctime==1.1.0
