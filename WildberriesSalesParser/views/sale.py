from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP, Float
from sqlalchemy.sql import func


class Sale(Base):

    __tablename__ = "sale_reports"

    id = Column(Integer, primary_key=True)

    product_size_id = Column(Integer, ForeignKey("product_sizes.id"),
                             nullable=False)

    brand = Column(String, nullable=True)
    category = Column(String, nullable=True)
    season = Column(String, nullable=True)
    collection = Column(String, nullable=True)
    name = Column(String, nullable=True)
    seller_article = Column(String, nullable=True)
    wb_article = Column(Integer, nullable=True)
    barcode = Column(String, nullable=True)
    size = Column(String, nullable=True)
    contract = Column(String, nullable=True)
    wb_storage_id = Column(String, nullable=True)
    income_count = Column(Integer, nullable=True)
    ordered_count = Column(Integer, nullable=True)
    ordered_sum = Column(Float, nullable=True)
    bought_count = Column(Integer, nullable=True)
    cach_to_send = Column(Float, nullable=True)
    current_remind = Column(String, nullable=True)
    report_created_date_from = Column(TIMESTAMP, default=datetime.utcnow(),
                                      nullable=False)
    report_created_date_to = Column(TIMESTAMP, default=datetime.utcnow(),
                                    nullable=False)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)
