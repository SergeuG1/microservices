from time import sleep
import datetime
import psycopg2
import requests
import json
import urllib.parse
from typing import List
from typing import Any
from dataclasses import dataclass
import urllib.parse
from loguru import logger
import asyncio
from asyncio import Semaphore
from aiohttp import ClientSession


def reload_request(url, headers):
    counter = 15
    while True:
        if counter == 0:
            return r
        try: 
            r = requests.get(
            url=url,
            headers=headers,
        )
            counter -= 1
        except Exception as e:
            logger.warning(e.args)
            continue

        if r.status_code == 200:
            break


    return r
     


def postion_products(keyword, page_limit:int = 60):

        search_key = urllib.parse.quote_plus(keyword)
        temp_key = []

        for page in range(1, page_limit+1):
            headers = {
                'Accept': '*/*',
                'Accept-Language': 'ru,en-US;q=0.9,en;q=0.8',
                'Connection': 'keep-alive',
                'Origin': 'https://www.wildberries.ru',
                'Referer': f'https://www.wildberries.ru/catalog/0/search.aspx?search={search_key}',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'cross-site',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 OPR/100.0.0.0',
                'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Opera GX";v="100"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
            }
            url = f'https://search.wb.ru/exactmatch/ru/common/v4/search?TestGroup=waterfall_card_hybrid_ads&TestID=216&appType=1&page={page}&curr=rub&dest=-1257786&query={search_key}&regions=80,38,83,4,64,33,68,70,30,40,86,75,69,22,1,31,66,110,48,71,114&resultset=catalog&sort=popular&spp=0&suppressSpellcheck=false'
            response = None
            try:
                response = requests.get(
                    url=url,
                    headers=headers,
                    timeout=20
                )
            except Exception as e:
                logger.warning(f"Ошибка {response} key = {keyword} на странице {page}")
                logger.warning(f"Ошибка {e.args} key = {keyword} на странице {page}\n")

            if response.status_code in [401, 403, 429, 500]:
                logger.warning(f"Ошибка {response.status_code} key = {keyword} на странице {page}")
                sleep(1)
                response = reload_request(url, headers)

                if response.status_code == 200:
                    logger.success(f"{response.status_code} данные по ключевому слову {keyword} на странице {page} были загружены")
                else:
                    logger.warning(f"{response.status_code} данные по ключевому слову {keyword} на странице {page} не были загружены")
                    continue

            data = response.json()
            if len(data) == 4 or len(data) == 0:
                logger.info(f"Конечная страница по ключевому слову {keyword} = {page}")
                break
            
            if "merger" in data.values():
                continue

            try:
                temp = data['data']['products']
            except Exception as e:
                logger.warning(f"Ошибка {response.status_code} key = {keyword} на странице {page}")
                logger.warning(f"Ошибка {e.args} key = {keyword} на странице {page}\n")
                continue
            
            temp_key.extend(temp)

        return temp_key



async def get_products(
        keyword: str,
        session: ClientSession,
        semaphore: Semaphore,
        page_limit: int,
        number_key: int):
    
    search_key = urllib.parse.quote_plus(keyword)
    temp_key = {keyword: []}
    async with semaphore:
            logger.info(keyword)
            for page in range(1, page_limit + 1):
                url = f'https://search.wb.ru/exactmatch/ru/common/v4/search?TestGroup=waterfall_card_hybrid_ads&TestID=216&appType=1&page={page}&curr=rub&dest=-1257786&query={search_key}&regions=80,38,83,4,64,33,68,70,30,40,86,75,69,22,1,31,66,110,48,71,114&resultset=catalog&sort=popular&spp=0&suppressSpellcheck=false'
                response = None
                try:
                    response = await session.get(url=url)
                except Exception as e:
                    logger.warning(e)
                    continue
                if response == None:
                    continue
                
                if response.status in [401, 403, 429, 500]:
                    while True:
                        sleep(1)
                        response = await session.get(url=url)
                        if response.status == 200:
                            logger.success(f"{response.status} данные по ключевому слову {keyword} на странице {page} были загружены")
                            break
                        else:
                            logger.warning(f"{response.status} данные по ключевому слову {keyword} на странице {page} не были загружены")
                try:
                    data = await response.json(content_type=None)
                except requests.Timeout as e:
                    logger.warning(e)
                    continue
                
                if "data" in data:
                    temp = data['data']['products']
                else:
                    break

                temp_key[keyword].extend(temp)
    logger.info(f"{keyword} {number_key}")

    return temp_key

# from time import sleep
# import datetime
# import psycopg2
# import requests
# import json
# import urllib.parse
# from typing import List
# from typing import Any
# from dataclasses import dataclass
# import urllib.parse
# from loguru import logger
# import asyncio
# from asyncio import Semaphore
# from aiohttp import ClientSession


# def reload_request(url, headers):
#     counter = 15
#     while True:
#         if counter == 0:
#             return r
#         try: 
#             r = requests.get(
#             url=url,
#             headers=headers,
#         )
#             counter -= 1
#         except Exception as e:
#             logger.warning(e.args)
#             continue

#         if r.status_code == 200:
#             break


#     return r
     


# def postion_products(keyword, page_limit:int = 60):

#         search_key = urllib.parse.quote_plus(keyword)
#         temp_key = []

#         for page in range(1, page_limit+1):
#             headers = {
#                 'Accept': '*/*',
#                 'Accept-Language': 'ru,en-US;q=0.9,en;q=0.8',
#                 'Connection': 'keep-alive',
#                 'Origin': 'https://www.wildberries.ru',
#                 'Referer': f'https://www.wildberries.ru/catalog/0/search.aspx?search={search_key}',
#                 'Sec-Fetch-Dest': 'empty',
#                 'Sec-Fetch-Mode': 'cors',
#                 'Sec-Fetch-Site': 'cross-site',
#                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 OPR/100.0.0.0',
#                 'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Opera GX";v="100"',
#                 'sec-ch-ua-mobile': '?0',
#                 'sec-ch-ua-platform': '"Windows"',
#             }
#             url = f'https://search.wb.ru/exactmatch/ru/common/v4/search?TestGroup=waterfall_card_hybrid_ads&TestID=216&appType=1&page={page}&curr=rub&dest=-1257786&query={search_key}&regions=80,38,83,4,64,33,68,70,30,40,86,75,69,22,1,31,66,110,48,71,114&resultset=catalog&sort=popular&spp=0&suppressSpellcheck=false'
#             response = None
#             try:
#                 response = requests.get(
#                     url=url,
#                     headers=headers,
#                     timeout=20
#                 )
#             except Exception as e:
#                 logger.warning(f"Ошибка {response} key = {keyword} на странице {page}")
#                 logger.warning(f"Ошибка {e.args} key = {keyword} на странице {page}\n")

#             if response.status_code in [401, 403, 429, 500]:
#                 logger.warning(f"Ошибка {response.status_code} key = {keyword} на странице {page}")
#                 sleep(1)
#                 response = reload_request(url, headers)

#                 if response.status_code == 200:
#                     logger.success(f"{response.status_code} данные по ключевому слову {keyword} на странице {page} были загружены")
#                 else:
#                     logger.warning(f"{response.status_code} данные по ключевому слову {keyword} на странице {page} не были загружены")
#                     continue

#             data = response.json()
#             if len(data) == 4 or len(data) == 0:
#                 logger.info(f"Конечная страница по ключевому слову {keyword} = {page}")
#                 break
            
#             if "merger" in data.values():
#                 continue

#             try:
#                 temp = data['data']['products']
#             except Exception as e:
#                 logger.warning(f"Ошибка {response.status_code} key = {keyword} на странице {page}")
#                 logger.warning(f"Ошибка {e.args} key = {keyword} на странице {page}\n")
#                 continue
            
#             temp_key.extend(temp)

#         return temp_key



# async def get_products(
#         keyword: str,
#         session: ClientSession,
#         semaphore: Semaphore,
#         page_limit: int,
#         number_key: int):
    
#     search_key = urllib.parse.quote_plus(keyword)
#     temp_key = {keyword: []}
#     async with semaphore:
#             logger.info(keyword)
#             for page in range(1, page_limit + 1):
#                 url = f'https://search.wb.ru/exactmatch/ru/common/v4/search?TestGroup=waterfall_card_hybrid_ads&TestID=216&appType=1&page={page}&curr=rub&dest=-1257786&query={search_key}&regions=80,38,83,4,64,33,68,70,30,40,86,75,69,22,1,31,66,110,48,71,114&resultset=catalog&sort=popular&spp=0&suppressSpellcheck=false'
#                 response = None
#                 try:
#                     response = await session.get(url=url)
#                 except Exception as e:
#                     logger.warning(e)
#                     continue
#                 if response.status in [401, 403, 429, 500]:
#                     stop = 5
#                     while True:
#                         if stop == 0:
#                             break
#                         stop -= 1
#                         sleep(1)
#                         try:
#                             response = await session.get(url=url)
#                         except Exception as e:
#                             logger.warning(e)
#                             continue
#                         if response.status == 200:
#                             logger.success(f"{response.status} данные по ключевому слову {keyword} на странице {page} были загружены")
#                             break
#                         else:
#                             logger.warning(f"{response.status} данные по ключевому слову {keyword} на странице {page} не были загружены")
#                 try:
#                     data = await response.json(content_type=None)
#                 except Exception as e:
#                     logger.warning(e)
#                     continue
                
#                 if "data" in data:
#                     temp = data['data']['products']
#                 else:
#                     break

#                 temp_key[keyword].extend(temp)

#     logger.info(f"{keyword} {number_key}")

#     return temp_key